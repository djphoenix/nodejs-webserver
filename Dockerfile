FROM node:6.14.2
ENV PORT 8080
EXPOSE ${PORT}
COPY server.js .
CMD node server.js
