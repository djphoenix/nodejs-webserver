function serve(port = 8080){
var http = require('http');

var handleRequest = function(request, response) {
  console.log('Received request for URL: ' + request.url);
  response.writeHead(200);
  response.end('Hello World from port ' + port + '!');
};
var www = http.createServer(handleRequest);
www.listen(port);
console.log('Ready on port: ' + port)
}
serve(process.env.PORT);
